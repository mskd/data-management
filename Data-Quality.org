# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2005-2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-FileCopyrightText: 2006-2019 Gottfried Vossen
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: data quality, data quality framework, data quality dimension, data profiling, data cleaning, duplicate,

* Introduction
** Learning Objectives
   :PROPERTIES:
   :CUSTOM_ID: learning-objectives-dq
   :reveal_extra_attr: data-audio-src="https://oer.gitlab.io/audio/learning-objectives.ogg"
   :END:
   - Explain data quality (DQ) as multidimensional concept
   - Identify DQ issues with reference to quality dimensions
     - Apply data profiling (as part of project work)
   - Apply cleaning process (as part of project work)
#+INCLUDE: "~/.emacs.d/oer-reveal-org/learning-objectives-notes.org"

** Motivating Examples
   :PROPERTIES:
   :CUSTOM_ID: dq-examples
   :reveal_extra_attr: data-audio-src="./audio/dq-examples.ogg"
   :END:
   - Who wrote this?
     - Lechtenberger, Jens Lechtenboerger, Jens Lechtenborger, Jens
       Lechtenb�rger, J. Lechtenbörger, ~lechten~
   - Where?
     - Grevener Straße 91, Steinfurter Str. 107, Steinfurter Straße
       109, Leonardo Campus 3, Leonardo-Campus 3
   - When?
     - 2020-11-01, Nov 1st, 1. November, 01/11, 11/01
   #+begin_notes
These sample values hint at different types of data quality issues.
Suppose that such values are recorded in different places at an
organization, where a single “correct” value should be used.

Please take a moment to think about the following question, which will
be revisited later on: How would you classify the data quality issues
concerning the author’s name?

The last two address variants indicate a spelling issue, which
concerns *syntax* and may therefore seem easy to resolve based on a
dictionary or registry of streets (where we would find
“Leonardo-Campus” but not “Leonardo Campus”).  With string similarity
measures, which are a topic later on, we might identify the correct
spelling.  (Indeed, when comparing the candidate ~Leonardo Campus~ to
known street names, ~Leonardo-Campus~ will be the most similar match.)

*Semantic* issues are much harder.  Indeed, /all/ shown addresses were
part of the author’s office location at different points in time
(namely, the Department of Information Systems at the University of
Münster in Germany).  Notably, the office just moved once, while we
see four different addresses (with a renumbering of buildings in one
street, where “Straße” is used inconsistently in abbreviated form
once, followed by a re-assignment of the building to a different
street name later on).  String similarity does not help here.

Besides simple typos, the examples here point to the lack of
/standards/ or /conventions/.  For example, higher data quality would
arise, if we answered the following questions ahead of time:
- Which abbreviations do we use?
- What string encoding (e.g., UTF-8)?
- What date format?

Similar considerations apply to data values in general.  /Data types/
and /integrity constraints/ of database management systems can enforce
some conventions and thereby prevent several types of errors.
   #+end_notes

*** An Impressive Example
   :PROPERTIES:
   :CUSTOM_ID: britney-spears
   :END:
   - See
     [[https://archive.google.com/jobs/britney.html][spelling variants for ~britney spears~ corrected by Google]]
     - Bottom line: Working with manual user input is challenging

** Why do we care?
   :PROPERTIES:
   :CUSTOM_ID: dq-quotes
   :END:
   - Quotes from cite:Mar05 (2005)
     - “88 per cent of all data integration projects either fail completely or
       significantly over-run their budgets”
     - “75 per cent of organisations have identified costs stemming from
       dirty data”
     - “33 per cent of organisations have delayed or cancelled new IT
       systems because of poor data”
     - “$611bn per year is lost in the US in poorly targeted mailings and
       staff overheads alone”
     - “According to Gartner, bad data is the number one cause of CRM
       system failure”
     - “Customer data typically degenerates at 2 per cent per month or 25
       per cent annually”

** Costs of Poor Data Quality
   :PROPERTIES:
   :CUSTOM_ID: dq-costs
   :END:
   - Attributing costs or assessing impact of poor quality is hard
     - See cite:HZVL11 for four types of costs
       - Two-by-two matrix
         - Costs may be direct or indirect
         - Quality affects operational tasks and strategic decisions
       - E.g., payment errors are direct and operational, poor
         production planning is indirect and strategic
     - Negative effects cited in cite:CR19
       - According to Gartner in 2018, organisations attribute losses of
         15 million USD per year on average
       - 2016 IBM research estimates total annual losses in US to be 3
         trillion USD
       - According to KPMG 2017 Global CEO Outlook, 56% of CEOs worry
         about negative impact on their decisions
       - Compliance risks

* Data Quality Frameworks
  :PROPERTIES:
  :CUSTOM_ID: dq-frameworks
  :END:

** DQ Framework Overview
   :PROPERTIES:
   :CUSTOM_ID: dq-framework-overview
   :END:
   - Data quality frameworks offer processes for strategic DQ improvement
   - See cite:CR19 for an overview
     - Comparison of twelve DQ frameworks that cover
       - DQ definition
       - DQ assessment
       - DQ improvement
     - Decision guide for organizations
       - Criteria to narrow down choice of framework

*** Data Quality (DQ)
    :PROPERTIES:
    :CUSTOM_ID: dq-definition
    :reveal_extra_attr: data-audio-src="./audio/dq-definition.ogg"
    :END:
    - “Fitness for use” (with background in quality literature, see cite:WS96)
      - Quality is judged by consumer according to context and purpose
    - Lots of data quality dimensions, see cite:SSP+12 for survey
      - According to cite:CR19, most commonly (going back to cite:WS96):
        - Completeness: Sufficient breadth, depth, scope for task at hand
        - Accuracy: Correct, reliable, certified
        - Timeliness: Age is appropriate for task at hand
        - Consistency: Same formats, compatible with previous data
        - Accessibility: Available, or easily and quickly retrievable
    #+begin_notes
As stated here, data quality is a multidimensional concept, for which
ultimately data consumers will judge whether the quality of a given
data set is fit, or good enough, for their intended use.

In the context of [[#dq-examples][data examples shown earlier]],
I asked how you would classify some DQ issues.
What were your thoughts back then?

Please revisit that slide with the dimensions mentioned here.  Which
ones are problematic for what purposes?  Anything else that comes to mind?
    #+end_notes

*** DQ Assessment
    :PROPERTIES:
    :CUSTOM_ID: dq-assessment
    :END:
    - Need metric per quality dimension
      - Subjective, e.g., measurements with surveys among data consumers
      - Objective, e.g., count NULL values, constraint violations,
        duplicates, or measure number of erroneous decisions

*** DQ Improvement
    :PROPERTIES:
    :CUSTOM_ID: dq-improvement
    :END:
    - Improve information products
      - E.g., de-duplicate data, fill in missing values, standardize,
        fix errors
        - See [[#kimball-process][cleaning process]] later on
    - Improve information processes
      - May start from root cause analysis
        - Why did low-quality data arise?
        - Change processes to avoid root causes.

* Data Profiling
  :PROPERTIES:
  :CUSTOM_ID: data-profiling
  :END:

** Getting to Know Your Data
   :PROPERTIES:
   :CUSTOM_ID: know-data
   :END:
   - See Chapter 3 in cite:HKP11
     - Inspect types of attributes
       - and their statistical properties such as mean, media, mode, quartiles, variance
     - Use visualizations
     - Along the way, identify data quality issues

** Data Profiling Aspects
   :PROPERTIES:
   :CUSTOM_ID: profiling-aspects
   :END:
   - Methodical inspection of data instead of manual “eye-balling”
   - See cite:AGN15 for a survey
     - “Data profiling is the set of activities and processes to
       determine the metadata about a given dataset.”
       - Single column, e.g., cardinalities, value distributions, data types
       - Multiple columns, e.g., correlations, topic overlap, duplicates
       - Dependencies, e.g., (foreign) keys, functional dependencies
         and their violations
   - Aside
     - [[https://hpi.de/fileadmin/user_upload/fachgebiete/naumann/publications/2017/SIGMOD_2017_Tutorial_Data_Profiling.pdf][SIGMOD 2017 tutorial slides]]
       based on cite:AGN15
       - With lists of industrial and research tools

** Talend Open Studio for Data Quality
   :PROPERTIES:
   :CUSTOM_ID: talend
   :END:
   - Sample tool with data profiling capabilities
     - Java, free/libre and open source (Apache License, Version 2.0)
     - [[https://sourceforge.net/projects/talendprofiler/][Project at SourceForge]]
   - See
     [[https://help.talend.com/reader/8taYQkblNoWRWJGmRtNq3g/iLaueZ1rs~0HidfQUN1sOQ][user’s guide for sample analysis results]]

* Sample Cleaning Process
  :PROPERTIES:
  :CUSTOM_ID: kimball-process
  :END:
  Kimball, Dealing with Dirty Data, DBMS, 1996
  - [[https://web.archive.org/web/19990209061653/http://www.dbmsmag.com/9609d14.html][Process description]]
    - Six steps, may be part of ETL process
      1. Elementizing
         - Split non-atomic values, e.g., names, addresses, dates
           (recall [[#dq-examples][initial examples]])
           - [[https://oer.gitlab.io/misc/Regular-Expressions.html][Regular expressions]] may help
      2. Standardizing (next slides)
      3. Verifying
         - Check whether elements are mutually consistent, e.g., ZIP
           code 48149 cannot be in Bavaria (08..., 09...)
         - Integrity constraints
      4. Matching
         - Check whether “equivalent” element does already exist; if
           yes, augment with new information (subsequent slides)
      5. Householding
         - Try to group elements, e.g., married couples
      6. Documenting

** Standardizing (1/2)
   :PROPERTIES:
   :CUSTOM_ID: standardizing-1
   :END:
   #+begin_leftcol
   - Consider “gender”
     - Varying source representations
       - Possibly “real” values mixed with salutations, academic titles
         - Possibly all of male/female/diverse/etc., m/w, m/f,
           Mann/Frau, Herr/Frau/Firma, Dipl.-Ing., Dr., Prof., ?, unknown,
           NULL
         - Use profiling results (e.g., count distinct, histogram)
     - Define convention, e.g.:
       -
         | TargetCode | TargetName |
         |------------+------------|
         |          0 | unknown    |
         |          1 | female     |
         |          2 | male       |
         |          3 | diverse    |

     - May use lookup table
       - Data type of column Source?
       - Join source data with lookup table
         - Beware of NULLs!
       - View or ETL process to produce target data
   #+end_leftcol
   #+begin_rightcol
   | Lookup | Data Source | Source  | TargetCode |
   |--------+-------------+---------+------------|
   |        | S1          | unknown |          0 |
   |        | S1          | female  |          1 |
   |        | S1          | male    |          2 |
   |        | S1          | diverse |          3 |
   |        | S2          | f       |          1 |
   |        | S2          | m       |          2 |
   |        | S2          | ?       |          0 |
   |        | S2          | NULL    |          0 |
   |        | S3          | 1       |          2 |
   |        | S3          | 2       |          1 |
   |        | ...         | ...     |        ... |
   #+end_rightcol

** Standardizing (2/2)
   :PROPERTIES:
   :CUSTOM_ID: standardizing-2
   :END:
   - NULL values are related to the completeness dimension of DQ
   - Avoid NULL values, explicitly represent degree of knowledge
     - *Three* types of NULLs
       - Not existing (inapplicable); no incompleteness issue
       - Value exists for sure, but we don’t know it; incomplete
       - We don’t know whether a value exists; unknown whether incomplete

   | ID | Name     | Surname | Birthdate  | E-Mail                       |
   |----+----------+---------+------------+------------------------------|
   |  1 | John     | Smith   | 03/17/1974 | smith@abc.it                 |
   |  2 | Edward   | Monroe  | 02/03/1967 | NULL (does not exist)        |
   |  3 | Anthony  | White   | 01/01/1936 | NULL (existing but unknown)  |
   |  4 | Marianne | Collins | 11/20/1955 | NULL (not known if existing) |

   #+ATTR_HTML: :class slide-source
   (Source: cite:SMB05)

** Matching
   :PROPERTIES:
   :CUSTOM_ID: matching
   :END:
   - *Matching* = object identification = duplicate detection
     - Matching is easy for exact duplicates or with “real” keys (e.g.,
       social security number)
       - For computers at least; see [[https://flic.kr/p/5ELLbe][this challenge for human beings]]
     - Otherwise, need *quasi-identifiers*
       - Groups of attributes, possibly with similarity matching for
         probabilistic matching
       - E.g., name, date of birth, and address in presence of
         spelling mistakes

*** Matching Outlook
   :PROPERTIES:
   :CUSTOM_ID: matching-outlook
   :END:
    - Finding of *similar items* to be revisited in several sessions
      - More efficient approaches than naive comparison of all pairs
        with quadratic complexity?
      - Measures/metrics for similarity?
    - Afterwards, *data fusion* is necessary
      - Given duplicates, create single object representation while
        resolving conflicting values

# Local Variables:
# oer-reveal-master: nil
# End:
