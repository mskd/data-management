<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2019-2020 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC0-1.0 -->

Org source files in this project are not meant to be exported directly
but to be included in small wrapper files that add title slide, theme,
etc.  See [presentations on Communication and Collaboration Systems](https://gitlab.com/oer/oer-courses/cacs)
and [presentations on miscellaneous topics](https://gitlab.com/oer/misc)
for examples.

BibTeX references in this project refer to
[this file](https://gitlab.com/oer/literature/-/blob/master/data-management.bib).
