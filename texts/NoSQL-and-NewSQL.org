# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2019-2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: database, NoSQL, NewSQL, CAP Theorem, consistency, eventual consistency, availability, partition tolerance, transaction, serializability, linearizability, scalability, Amdahl, Gustafson,

#+INCLUDE: "../../texts/config.org"

#+TITLE: NoSQL and NewSQL

* Context
  This session presents newer types of data management systems, namely
  [[#nosql][NoSQL systems]] and [[#newsql][NewSQL database systems]],
  which are frequently associated with big data settings.  Big data
  may be characterized by 5 Vs cite:GBR15, namely large Volume, high
  Velocity (speed of data production and processing), diverse Variety
  (data from different sources may be heterogeneous; structured,
  semi-structured, or unstructured), unknown Veracity (raising
  concerns related to data quality and trust), potential Value (e.g.,
  as basis for business intelligence or analytics).  NoSQL and NewSQL
  address those Vs differently: Both promise scalability, addressing
  Volume and Velocity.  NoSQL systems come with flexible data models,
  addressing Variety.  NewSQL database systems perform ACID
  transactions, addressing Veracity.  Value is up for discussion.

  This text is meant to introduce you to general concepts that are
  relevant in videos in Learnweb (and beyond) and to point you to
  current research.

** Learning Objectives
   - Apply Amdahl’s and Gustafson’s laws to reason about speedup under
     parallelization.
   - Explain the notions of consistency in database contexts and of
     eventual consistency under quorum replication.
   - Explain the intuition of the CAP Theorem and discuss its implications.
   - Explain objectives and characteristics of “NoSQL” and “NewSQL”
     systems in view of big data scenarios.

* Background
** Scalability
   A system is /scalable/ if it benefits “appropriately” from an
   increase in compute resources.  E.g., if resources are increased by
   a factor of 2, throughput should be about twice as high for a
   system that scales linearly.

   Scaling of computers comes in two major variants, namely /scaling up/
   (also called /scaling vertically/) and /scaling out/ (also called
   /scaling horizontally/ or /sharding/).

   When scaling up (see Figure [[fig-scale-up]]), we improve resources
   of a single machine (e.g., we add more RAM, more/faster CPU cores).
   Clearly, the potential of this type of scaling is limited (e.g., by
   sockets on the mainboard) and costly.

   #+CAPTION: Scaling up improves the performance of a single machine.
   #+NAME: fig-scale-up
   [[./scale-up.png]]

   When scaling out (see Figure [[fig-scale-out]]), we add more
   machines and distribute the load over those machines subsequently,
   for example with [[file:OLAP-optimization.org][horizontal fragmentation]].
   This approach is not limited by physical restrictions of any single
   machine and is the usual one in the context of big data.  Indeed,
   cite:CP14 characterizes big data as being
   “large enough to benefit significantly from parallel computation
   across a fleet of systems, where the efficient orchestration of the
   computation is itself a considerable challenge.”

   #+CAPTION: Scaling out distributes load over a fleet of systems.
   #+NAME: fig-scale-out
   [[./scale-out.png]]

** Limits to Scalability
   :PROPERTIES:
   :CUSTOM_ID: amdahl
   :END:
   Amdahl’s law cite:Amd67 provides an upper bound for the speedup
   that can be gained by parallelizing computations which (also)
   include a non-parallelizable portion.  More specifically, if some
   computation is parallelized to $N$ machines and involves a serial
   fraction $s$ that cannot be parallelized, the speedup is limited as
   follows:

   Speedup = $\frac{1}{s + \frac{1-s}{N}}$

   E.g., consider a computation of which 90% are parallelizable (i.e.,
   $s=0.1$), to be executed on $N=10$ machines.  You might guess the
   speedup to be close to 90% of 10, while you find only 5.3 given the
   above equation.  Indeed, note that for $N$ approaching infinity the
   fraction $\frac{1-s}{N}$ disappears; for $s=0.1$, the speedup is
   limited by 10.  I was quite surprised when I first saw that law’s
   results.

   <<gustafson>>
   Gustafson’s law cite:Gus88 can be interpreted as counter-argument
   to Amdahl’s law, where he starts from the observation that larger
   problem instances are solved with better hardware.  In contrast,
   Amdahl considers a /fixed/ problem size when estimating speedup:
   Whereas Amdahl’s law asks to what fraction one time unit of
   computation can be /reduced/ with parallelism, Gustafson’s law
   starts from one time unit under parallelism with $N$ machines and
   asks to how many time units of sequential computation it would be
   /expanded/ with this computation:

   Speedup = $s + (1-s)N$

   Given $s=0.1$ and $N=100$ we now find a speedup of 90.1.
   Apparently, this change of perspective leads to dramatically
   different outcomes.  Nevertheless, if your goal is to speed up a
   given computation, Amdahl’s law is the appropriate one (and it
   implies that serial code should be reduced as much as possible; for
   example, your implementation should not serialize commutative
   operations such as [[commutativity][deposit]] below).

** Databases
   :PROPERTIES:
   :CUSTOM_ID: database
   :END:
   “Database” is an overloaded term.  It may refer to a collection of
   data (e.g., the
   [[https://gateway.euro.who.int/en/datasets/european-mortality-database/][“European mortality database”]]),
   to software (e.g., [[https://www.postgresql.org/][“PostgreSQL: The world's most advanced open source database”]]),
   or to a system with hardware, software, and data (frequently a
   [[https://oer.gitlab.io/oer-courses/cacs/Distributed-Systems-Introduction.html][distributed system]]
   involving lots of physical machines).  If we want to be more precise,
   we may refer to the software managing data as /database management
   system/ (DBMS), the data managed by that software as /database/ and
   the combination of both as /database system/.

   Over the past decades, we have come to expect certain properties from
   database systems (that distinguish them from, say, file systems), including:
   - /Declarative query languages/ (e.g., SQL for relational data,
     XQuery for XML documents, SPARQL for RDF and graph-based data)
     allow us to declare how query results should look like, while we
     do /not/ need to specify or program what operations to execute in
     what order to accumulate desired results.
   - /Data independence/ shields applications and users from details of
     physical data organization in terms of bits, bytes, and access
     paths.  Instead, data access is organized and expressed in terms
     of a /logical schema/, which remains stable when underlying
     implementation or optimization aspects change.
   - /Database transactions/ as sequences of database operations
     maintain a consistent database state with ACID guarantees (the
     acronym ACID was coined by Haerder and Reuter in cite:HR83,
     building on work by Gray cite:Gra81):
     - Atomicity: Either all operations of a transaction are executed
       or none leaves any effect; recovery mechanisms ensure that effects
       of partial executions (e.g., in case of failures) are undone.
     - Consistency: Each transaction preserves integrity constraints.
     - Isolation: Concurrency control mechanisms ensure that each
       transaction appears to have exclusive access to the database
       (i.e., [[https://oer.gitlab.io/OS/Operating-Systems-MX.html#slide-race-condition][race conditions]]
       such as dirty reads and lost updates are avoided).
     - Durability: Effects of successfully executed transactions are
       stored persistently and survive subsequent failures.

** Consistency
   The “C” for Consistency in ACID is not our focus here.  Instead,
   /serializability/ cite:Pap79 is the classical yardstick for
   /consistency/ in transaction processing contexts (addressed by the
   “I” for Isolation in ACID).  In essence, some concurrent execution
   (usually called /schedule/ or /history/) of operations from
   different transactions is /serializable/, if it is “equivalent” to
   some serial execution of the same transactions.  (As individual
   transactions preserve consistency (the “C”), by
   [[https://en.wikipedia.org/wiki/Mathematical_induction][induction]]
   their serial execution does so as well.  Hence, histories that are
   equivalent to serial ones are “fine”.)

   A basic way to define “equivalence” is /conflict-equivalence/ in
   the read-write or page model of transactions (see cite:WV02 for
   details, also on other models).  Here, transactions are /sequences/
   of read and write operations on non-overlapping objects/pages, and
   we say that the pair (o_1, o_2) of operations from different
   transactions is /in conflict/ if (a) o_1 precedes o_2, (b) they
   involve the same object, and (c) at least one operation is a write
   operation.  Two histories involving the same transactions are
   /conflict-equivalent/ if they contain the same conflict pairs.  In
   other words, conflicting operations need to be executed in the same
   order in equivalent histories, which implies that their results are
   the same in equivalent histories.

   Note that serializable histories may only be equivalent to
   counter-intuitive serial executions as the following history h
   (adapted from an example in cite:Pap79) shows, which involves read
   (R) and write (W) operations from three transactions (indicated by
   indices on operations) on objects x and y:

   h = R_1[x] W_2[x] W_3[y] W_1[y]

   Here, we have conflicting pairs (R_1[x], W_2[x]) and (W_3[y],
   W_1[y]).  The only serial history with the same conflicts is h_S:

   h_S = W_3[y] R_1[x] W_1[y] W_2[x]

   Papadimitriou cite:Pap79 observed: “What is interesting is that in
   h transaction 2 has completed execution before transaction 3 has
   started executing, whereas the order in h_S has to be the reverse.
   This phenomenon is quite counterintuitive, and it has been thought
   that perhaps the notion of correctness in transaction systems has
   to be strengthened so as to exclude, besides histories that are not
   serializable, also histories that present this kind of behavior.”

   <<linearizability>>
   He then went on to define /strict serializability/ where such
   transaction orders must be respected.  Later on, Herlihy and Wing
   cite:HW90 defined the notion of /linearizability/, which formalizes
   a similar idea for operations on /abstract data types/.  In our
   context, linearizability is the formal notion of consistency used
   in the famous CAP Theorem, which is frequently cited along with
   [[#nosql][NoSQL systems]].

   <<commutativity>> As a side remark, for abstract data types, we can
   reason about /commutativity/ of operations to define conflicts: Two
   operations conflict, if they are /not/ commutative.  For example, a
   balance check operation and a deposit operation on the same bank
   account are not commutative as the account’s balance differs before
   and after the deposit operation.
   In contrast, two deposit operations on a bank account both change the
   account’s state (and would therefore be considered conflicting in
   the read-write model), but they are not in conflict as their order
   does not matter for the final balance.  Hence, there is no need to
   serialize the order of such commutative operations.

   <<eventual-consistency>> In the NoSQL context, /eventual
   consistency/ is a popular relaxed version of consistency.  The
   intuitive understanding expressed in cite:BG13 is good enough for
   us: “Informally, it guarantees that, if no additional updates are
   made to a given data item, all reads to that item will eventually
   return the same value.”

   <<calm>> As a pointer to recent research that foregoes
   serializability and linearizability, I recommend Hellerstein and
   Alvaro cite:HA20, who review an approach based on the so-called
   CALM theorem (for Consistency As Logical Monotonicity)
   towards consistency /without/ coordination.  To appreciate that
   work, note first that coordination implies serial computation in
   the sense of [[#amdahl][Amdahl’s law]], which limits scalability.
   Thus, not having to endure coordination is a good thing.  Second,
   the approach allows us to design computations for which /eventual
   consistency/ is actually safe.  (The general idea is based on
   observing “consistent” overall outcomes of local computations,
   similarly to the above [[commutativity][commutativity]] argument
   but based on monotonicity of computations.)

* NoSQL
  :PROPERTIES:
  :CUSTOM_ID: nosql
  :END:
  NoSQL is an umbrella term for a variety of systems that may or may
  not exhibit the above database properties.  Hence, the term “NoSQL
  data store” used in the survey articles cite:Cat11 and cite:DCL18
  seems more appropriate than “NoSQL database”.  (In the past, I
  talked about “NoSQL databases”, which you might hear in videos;
  nowadays, I try to avoid that term.)

  Usually, NoSQL is spelled out as “Not only SQL”, which is somewhat
  misleading as several NoSQL systems are unrelated to SQL.
  Nevertheless, that interpretation stresses the observation that SQL
  may not be important for all types of applications.

  The NoSQL movement arose around 2005-2009 where we saw several
  web-scale companies running their home-grown data management systems
  instead of established (relational) database systems.  Google’s
  Bigtable cite:CDG+06 and Amazon’s Dynamo cite:DHJ+07 were seminal
  developments in the context of that movement.

  More generally, NoSQL systems advertise simplicity (instead of the
  complexities of SQL), flexibility (free/libre and open source
  software with bindings into usual programming languages,
  accommodating unstructured, semi-structured, or evolving data),
  scaling out, and availability.  Nowadays, NoSQL subsumes a variety
  of data models (key-value, document, graph, and column-family) for
  increased flexibility and focuses on scalability and availability,
  while consistency guarantees are typically reduced (see cite:DCL18
  for a survey and [[https://hostingdata.co.uk/nosql-database/]] for a
  catalog of more than 200 NoSQL systems as of December 2020).

  From a conceptual perspective, the CAP Theorem (introduced as
  conjecture in cite:Bre00; formalized theorem proven in cite:GL02)
  expresses a trade-off between Consistency and Availability in the
  case of network Partitions.  The definitions used for the theorem
  and its proof may not be what you need or expect in your data
  management scenarios as argued in this
  [[https://martin.kleppmann.com/2015/05/11/please-stop-calling-databases-cp-or-ap.html][blog post by Martin Kleppmann]].

  Regardless of that critique, the trade-off expressed by the CAP
  Theorem between consistency and availability is real, is attributed
  to cite:RG77 in cite:DCL18, and should not be too surprising: If a
  network partition does not allow different parts of the system to
  communicate with each other, they cannot synchronize their states
  any longer.  Thus, if different parts continue to be available and
  to apply updates (e.g., from local clients), their states will
  diverge, violating usual notions of consistency (such as
  [[linearizability][linearizability]], which is used in the proof of
  the CAP theorem, while in a video I phrased consistency as “all
  copies have the same value”).  Alternatively, some parts could shut
  down to avoid diverging states until the partition is resolved,
  violating availability.

  Against this background, NoSQL systems frequently aim to improve
  availability by offering a relaxed notion of consistency, which
  deviates from the transactional guarantees of older SQL systems as
  well as of newer NewSQL databases.

  Note that consistency under failure situations is a complicate
  matter, and lots of vendors promise more than their systems comply
  with.  See the
  [[https://aphyr.com/tags/jepsen][blog posts by Kyle Kingsbury]]
  for failures discovered with the test library
  [[https://github.com/jepsen-io/jepsen][Jepsen]], which runs
  operations against distributed systems under controlled failure
  situations.

* NewSQL
  :PROPERTIES:
  :CUSTOM_ID: newsql
  :END:
  NewSQL (see cite:PA16 for a survey) can be perceived as
  counter-movement from NoSQL back to the roots of relational database
  systems (prominently advocated by
  [[https://blogs.451research.com/information_management/2011/04/06/what-we-talk-about-when-we-talk-about-newsql/][Aslett]]
  and
  [[https://cacm.acm.org/blogs/blog-cacm/109710-new-sql-an-alternative-to-nosql-and-old-sql-for-new-oltp-apps/fulltext][Stonebreaker]]
  in 2011).

  In brief, NewSQL systems are database systems in the above sense,
  which comes with two major strengths:
  1. Declarative querying based on standards and data independence
     boost developer productivity.
  2. Business applications frequently require highly consistent data,
     as managed with ACID transactions.

  In addition, NewSQL database systems demonstrate that horizontal
  scalability and high availability can be achieved for high-volume
  relational data with SQL.

* Tasks
** Self-study
-  Watch the provided videos and ask any questions you may have.
   -  The video on Partitioning and Replication ends with a sample
      scenario. Convince yourself that the classification of queries and
      update into single-partition and multi-partition is correct.
   -  The video on Quorum Replication ends with a question concerning
      the case W=2 and R=1 if a read operation comes in after a write
      operation took place. What cases can you distinguish? How does the
      situation change for R=2?
   -  After the video on Vector Clocks, explain how the coordinator for
      Quorum Replication with N=3, W=2, R=2 can choose the most recent
      version for a read operation.
   -  What trade-off is expressed by the CAP Theorem?
   -  What techniques does F1 employ to offer availability?

** In-class
Tasks in this section will be discussed in class.

1. Compare results for self-study tasks.
2. Discuss in view of the CAP Theorem: "On the Web, strong consistency
   is not possible for highly available systems. [...] So, eventual
   consistency is the best we can go for."
3. Questions on Exercise Sheet 3.

#+INCLUDE: "../../texts/backmatter.org"
